shelf=[]
jk=input()
jj=jk.split()
no_of_books=int(jj[0])
maximum_chp=int(jj[1])

if(no_of_books<=10**5 and no_of_books > 0 and maximum_chp>0 and maximum_chp<=10**9):
    for x in range(no_of_books):
        chapters_in_book=int(input())
        shelf.append(chapters_in_book)

    for p in range(len(shelf)):
        if(shelf[0] <= maximum_chp):
            shelf.pop(0)
        else:
            break
    for u in range(len(shelf)):
        if(shelf[-1] <= maximum_chp):
          shelf.pop(-1)
        else:
            break  
    print(len(shelf))
else:
    print('please follow these constraints\n1<=no of books <=10^5\n1<=maximum chapters in book <=10^9')