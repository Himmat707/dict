users=[]
movies=[]
booked=[]
print("""press 1 to register as user
press 2 to register as admin
press 3 to add movie
press 4 to view avialble movies
press 5 to book ticket
press 6 to see bookings
press 0 to exit""")
def registeruser():
    name=input('please enter your name : ')
    mno=input('please enter your mobile no : ')
    pin=input('please enter your pin')
    flag1=True
    if len(mno)==10:
        for x in users:
            if x['mno'] == mno:
                flag1=False
        if flag1:
            users.append({'name':name , 'mno':mno ,'pin':pin , 'type':'u'})
            print('Thanku {} you are registered as user and your pin is {}'.format(name,pin))
        else:
            print('This mobile no already exists in database')
    else:
        print('Enter valid mobile no')
def registeradmin():
    name=input('please enter your name : ')
    mno=input('please enter your mobile no : ')
    pin=input('please enter your pin')
    flag2=True
    if len(mno)==10:
        for x in users:
            if x['mno'] == mno:
                flag2=False
        if flag2:
            users.append({'name':name , 'mno':mno ,'pin':pin , 'type':'a'})
            print('Thanku {} you are registered as admin and your pin is {}'.format(name,pin))
        else:
            print('This mobile no already exists in database')
    else:
        print('Enter valid mobile no')
def addmovie():
    pin=input('please enter your pin : ')
    flag3 =True
    ty=''
    for x in users:
        if x['pin']==pin:
            ty=x['type']
            nm=x['name']
            flag3=False
    
    if flag3==False and ty=='u':
        print('user can not add movies!!')
    elif flag3==True:
        print('invalid pin!!!')
    elif flag3==False and ty=='a':
        moviename=input('Please enter movie name:')
        lang=input('enter movies language')
        totals=int(input('please enter total seats'))
        avial=int(input('please enter avialable seats'))
        movies.append({'moviename':moviename , 'totalseats':totals ,'avial':avial ,'language':lang })
        print('your movie {} is added with total seats {} and avialble seats {}'.format(moviename,totals,avial))
def booktickets():
    if len(movies)>0:
        pin=input('enter your pin')
        flag4 =True
        ty=''
        for x in users:
            if x['pin']==pin:
                ty=x['type']
                nm=x['name']
                flag4=False
        
        if flag4==False and ty=='a':
            print('admin can not book movies!!')
        elif flag4==True:
            print('invalid pin!!!')
        elif flag4==False and ty=='u':
            moviename=input('Please enter movie name:')
            seats=int(input('please enter total seats you want to book'))
            flag5=True
            for x in movies:
                if x['moviename']==moviename:
                    al=x['avial']
                    ll=x['language']
                    flag5=False
            if flag5==False and al>=seats:
                for x in movies:
                    if x['moviename']==moviename:
                        x['avial']= x['avial']-seats
                booked.append({'name':nm , 'moviename':moviename , 'bookedseats':seats , 'language':ll })
                print('your movie {} is booked with {} seats'.format(moviename,seats))
            elif flag5== False and al <seats:
                print('avialble seats are less than your selection')
            elif flag5==True:
                print('no such movie found')
    else:
        print('no movies avialble now for booking')
def seebooking():
    if len(booked)>0:
        for x in booked:
            print(x['name'],x['moviename'],x['bookedseats'],x['language'])
    else:
        print('no bookings yet')
while True:
    option=input('choose any option')
    if(option=='1'):
        registeruser()
    elif(option=='2'):
        registeradmin()
    elif(option=='3'):
        addmovie()
    elif(option=='4'):
        for x in movies:
            print(x)
    elif(option=='5'):
        booktickets()
    elif(option=='6'):
        seebooking()
    elif(option=='0'):
        break
